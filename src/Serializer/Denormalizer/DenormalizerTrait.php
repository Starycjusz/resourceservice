<?php

namespace DS\DataProvider\Serializer\Denormalizer;

/**
 * DenormalizerTrait
 *
 * converts only camelCase format!
 */
trait DenormalizerTrait
{
    /**
     * @var array
     */
    public static $methodsMap = [];

    /**
     * DenormalizerTrait
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $property => $value) {
            if (!isset(self::$methodsMap[$property])) {
                $availableSetters = $this->findSetters($property);
                foreach ($availableSetters as $setter) {
                    if (method_exists($this, $setter)) {
                        self::$methodsMap[$property] = $setter;
                    }
                }
            }

            if (isset(self::$methodsMap[$property])) {
                $setter = self::$methodsMap[$property];

                $this->$setter($value);
            }
        }
    }

    /**
     * @param string $property
     *
     * @return array<string>
     */
    protected function findSetters(string $property): array
    {
        return [
            'set'.ucfirst($property),
        ];
    }
}
