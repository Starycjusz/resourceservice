<?php

namespace DS\DataProvider\Http;

/**
 * DummyHttpClient
 *
 * Do NOT reinventing the wheel - go get some good packages, i.e. gazzlehttp/gazzle
 */
class DummyHttpClient implements HttpClientInterface
{
    /**
     * {@inheritdoc}
     */
    public function call(string $method, string $url, array $data = [])
    {
        $curl = curl_init();

        switch ($method) {
            case 'POST':
                curl_setopt($curl, CURLOPT_POST, 1);
                $data && curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                break;
            case 'PUT':
                curl_setopt($curl, CURLOPT_PUT, 1);
                $data && curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                break;
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($curl), true);

        curl_close($curl);

        return $result;
    }
}
