<?php

namespace DS\DataProvider\Http;

/**
 * Interface HttpClientInterface
 */
interface HttpClientInterface
{
    /**
     * @param string $method
     * @param string $url
     * @param array  $data
     *
     * @return mixed
     */
    public function call(string $method, string $url, array $data = []);
}