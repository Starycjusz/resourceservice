<?php

namespace DS\DataProvider\Service;

use DS\DataProvider\Exception\InvalidConfigurationException;
use DS\DataProvider\Http\HttpClientInterface;

/**
 * AbstractResourceService
 */
abstract class AbstractResourceService
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @param string              $url
     * @param HttpClientInterface $httpClient
     */
    public function __construct(string $url, HttpClientInterface $httpClient)
    {
        $this->url = $url;
        $this->httpClient = $httpClient;
    }

    /**
     * {@inheritdoc}
     */
    public function get(): iterable
    {
        $response = $this->getHttpClient()->call('GET', $this->url) ?? [];

        foreach ($response as $item) {
            yield $this->denormalize($item);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getOneOrNull(int $id)
    {
        $response = $this->getHttpClient()->call('GET', vsprintf('%s/%d', [$this->url, $id]));

        if ($response) {
            return $this->denormalize($response);
        }

        return null;
    }

    /**
     * @param int $id
     */
    public function remove(int $id): void
    {
        $this->getHttpClient()->call('DELETE', vsprintf('%s/%d', [$this->url, $id]));
    }

    /**
     * @param array $data
     */
    protected function createFromArray(array $data): void
    {
        $this->getHttpClient()->call('POST', $this->url, $data);
    }

    /**
     * @param array $data
     */
    protected function updateFromArray(int $id, array $data): void
    {
        $this->getHttpClient()->call('PUT', vsprintf('%s/%d', [$this->url, $id]), $data);
    }

    /**
     * @return HttpClientInterface
     *
     * @throws InvalidConfigurationException
     */
    protected function getHttpClient(): HttpClientInterface
    {
        if ($this->httpClient) {
            return $this->httpClient;
        }

        throw new InvalidConfigurationException('Inject valid HttpClient.');
    }

    /**
     * @return string
     */
    protected function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Denormilize given data to an object
     *
     * @param array $data
     *
     * @return mixed
     */
    abstract protected function denormalize(array $data = []);
}
