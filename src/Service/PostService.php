<?php

namespace DS\DataProvider\Service;

use DS\DataProvider\Http\DummyHttpClient;
use DS\DataProvider\Http\HttpClientInterface;
use DS\DataProvider\Model\Post;

/**
 * PostService
 */
class PostService extends AbstractResourceService
{
    /**
     * @param string|null              $url
     * @param HttpClientInterface|null $httpClient
     */
    public function __construct(string $url = null, HttpClientInterface $httpClient = null)
    {
        // TODO: Get this shit from IoC container
        parent::__construct(
            $url ?? 'https://jsonplaceholder.typicode.com/posts',
            $httpClient ?? new DummyHttpClient()
        );
    }

    /**
     * @param int $id
     *
     * @return Post|null
     */
    public function getOneOrNull(int $id): ?Post
    {
        return parent::getOneOrNull($id);
    }

    /**
     * @param Post $post
     */
    public function create(Post $post): void
    {
        parent::createFromArray($this->normalize($post));
    }

    /**
     * @param Post $post
     */
    public function update(Post $post): void
    {
        parent::updateFromArray($post->getId(), $this->normalize($post));
    }

    /**
     * @param array $data
     *
     * @return Post
     */
    protected function denormalize(array $data = []): Post
    {
        return new Post($data);
    }

    protected function normalize(Post $post): array
    {
        return [
            'id'     => $post->getId(),
            'userId' => $post->getUserId(),
            'title'  => $post->getTitle(),
            'body'   => $post->getBody(),
        ];
    }
}
