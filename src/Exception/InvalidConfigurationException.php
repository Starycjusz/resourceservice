<?php

namespace DS\DataProvider\Exception;

/**
 * InvalidConfigurationException
 */
class InvalidConfigurationException extends \Exception
{
}
