<?php

namespace DS\DataProvider\Tests\Serializer\Denormalizer;

use DS\DataProvider\Serializer\Denormalizer\DenormalizerTrait;

/**
 * Foo
 * test class for testing DenormalizerTrait
 */
class Foo
{
    use DenormalizerTrait;

    /**
     * @var int
     */
    private $property1;

    /**
     * @var string
     */
    private $property2;

    /**
     * @var bool
     */
    private $property3;

    /**
     * @return int
     */
    public function getProperty1(): int
    {
        return $this->property1;
    }

    /**
     * @param int $property1
     *
     * @return Foo
     */
    public function setProperty1(int $property1): Foo
    {
        $this->property1 = $property1;

        return $this;
    }

    /**
     * @return string
     */
    public function getProperty2(): string
    {
        return $this->property2;
    }

    /**
     * @param string $property2
     *
     * @return Foo
     */
    public function setProperty2(string $property2): Foo
    {
        $this->property2 = $property2;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasProperty3(): bool
    {
        return $this->property3;
    }

    /**
     * @param bool $property3
     *
     * @return Foo
     */
    public function setProperty3(bool $property3): Foo
    {
        $this->property3 = $property3;

        return $this;
    }
}
