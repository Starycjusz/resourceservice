<?php

namespace DS\DataProvider\Tests\Serializer\Denormalizer;

use DS\DataProvider\Tests\TestCase\AbstractTestCase;
use ReflectionClass;

/**
 * DenormalizerTraitTest
 */
class DenormalizerTraitTest extends AbstractTestCase
{
    /**
     * testing denormalization process on Foo class
     */
    public function testDenormalization()
    {
        // Arrange
        $arrayData = [
            'property1' => rand(1, 100),
            'property2' => uniqid(),
            'property3' => (bool) rand(0, 1),
        ];

        // Act
        $result = new Foo($arrayData);

        // Assert
        $this->assertSame($arrayData['property1'], $result->getProperty1());
        $this->assertSame($arrayData['property2'], $result->getProperty2());
        $this->assertSame($arrayData['property3'], $result->hasProperty3());
    }

    /**
     * testing setting mutator before denormalizing
     */
    public function testSetMutator()
    {
        // Arrange
        Foo::$methodsMap['property1'] = 'setProperty2';

        $arrayData = [
            'property1' => uniqid(),
        ];

        // Act
        $result = new Foo($arrayData);

        // Assert
        $this->assertSame($arrayData['property1'], $result->getProperty2());
    }

    /**
     * testing skipping unsupported properties (instead of generating errors)
     */
    public function testSkipUnsupportedProperties()
    {
        // Arrange
        $key = uniqid();
        $value = uniqid();

        $arrayData = [
            $key => $value,
        ];

        // Act
        $result = new Foo($arrayData);

        $reflection = new ReflectionClass($result);
        $properties = $reflection->getProperties();

        foreach ($properties as $property) {
            $property->setAccessible(true);

            // Assert
            $this->assertNotSame($value, $property->getValue($result));
        }
    }

    /**
     * testing if can create instance without providing any data
     */
    public function testNotRequiringData()
    {
        // Act
        $result = new Foo();

        // Assert
        $this->assertInstanceOf(Foo::class, $result);
    }
}
