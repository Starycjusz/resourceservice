<?php

namespace DS\DataProvider\Tests\Service;

use DS\DataProvider\Model\Post;
use DS\DataProvider\Service\PostService;
use DS\DataProvider\Tests\TestCase\AbstractTestCase;

/**
 * PostServiceTest
 */
class PostServiceTest extends AbstractTestCase
{
    /**
     * testing getting post collection
     */
    public function testGet()
    {
        // Arrange
        $url = uniqid();

        $response = [$this->createRandomPostData(), $this->createRandomPostData(), $this->createRandomPostData()];

        $httpClient = $this->getHttpMock();
        $httpClient
            ->expects($this->once())->method('call')->with('GET', $url)->willReturn($response);

        $service = new PostService($url, $httpClient);

        // Act
        $result = $service->get();

        // Assert
        $this->assertTrue(is_iterable($result));

        /** @var Post $post */
        foreach ($result as $index => $post) {
            $this->assertInstanceOf(Post::class, $post);

            $this->assertSame($response[$index]['id'], $post->getId());
            $this->assertSame($response[$index]['userId'], $post->getUserId());
            $this->assertSame($response[$index]['title'], $post->getTitle());
            $this->assertSame($response[$index]['body'], $post->getBody());
        }
    }

    /**
     * testing getting single post
     */
    public function testGetOne()
    {
        // Arrange
        $id = rand(1, 100);
        $url = uniqid();

        $response = $this->createRandomPostData();

        $httpClient = $this->getHttpMock();
        $httpClient
            ->expects($this->once())
            ->method('call')
            ->with('GET', vsprintf('%s/%d', [$url, $id]))
            ->willReturn($response);

        $service = new PostService($url, $httpClient);

        // Act
        $result = $service->getOneOrNull($id);

        // Assert
        $this->assertInstanceOf(Post::class, $result);

        $this->assertSame($response['id'], $result->getId());
        $this->assertSame($response['userId'], $result->getUserId());
        $this->assertSame($response['title'], $result->getTitle());
        $this->assertSame($response['body'], $result->getBody());
    }

    /**
     * testing getting single post - when there is none
     */
    public function testGetNull()
    {
        // Arrange
        $id = rand(1, 100);
        $url = uniqid();

        $httpClient = $this->getHttpMock();
        $httpClient
            ->expects($this->once())
            ->method('call')
            ->with('GET', vsprintf('%s/%d', [$url, $id]))
            ->willReturn(null);

        $service = new PostService($url, $httpClient);

        // Act
        $result = $service->getOneOrNull($id);

        // Assert
        $this->assertNull($result);
    }

    /**
     * testing creating new post
     */
    public function testCreate()
    {
        // Arrange
        $url = uniqid();

        $postData = $this->createRandomPostData();

        $httpClient = $this->getHttpMock();
        $httpClient
            ->expects($this->once())->method('call')->with('POST', $url, $postData);

        $service = new PostService($url, $httpClient);

        // Act
        $service->create(new Post($postData));
    }

    /**
     * testing updating post
     */
    public function testUpdate()
    {
        // Arrange
        $url = uniqid();

        $postData = $this->createRandomPostData();

        $httpClient = $this->getHttpMock();
        $httpClient
            ->expects($this->once())
            ->method('call')
            ->with('PUT', vsprintf('%s/%d', [$url, $postData['id']]), $postData)
            ->willReturn(null);

        $service = new PostService($url, $httpClient);

        // Act
        $service->update(new Post($postData));
    }

    /**
     * testing deleting post
     */
    public function testDelete()
    {
        // Arrange
        $id = rand(1, 100);
        $url = uniqid();

        $httpClient = $this->getHttpMock();
        $httpClient
            ->expects($this->once())->method('call')->with('DELETE', vsprintf('%s/%d', [$url, $id]));

        $service = new PostService($url, $httpClient);

        // Act
        $service->remove($id);
    }

    /**
     * @return array
     */
    private function createRandomPostData(): array
    {
        return [
            'id'     => rand(1, 100),
            'userId' => rand(1, 100),
            'title'  => uniqid(),
            'body'   => uniqid(),
        ];
    }
}
