<?php

namespace DS\DataProvider\Tests\TestCase;

use DS\DataProvider\Http\DummyHttpClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * AbstractTestCase
 */
class AbstractTestCase extends TestCase
{
    /**
     * @param string $class
     *
     * @return MockObject
     */
    protected function getServiceMock(string $class): MockObject
    {
        return $this->getMockBuilder($class)->disableOriginalConstructor()->getMock();
    }

    /**
     * @return MockObject
     */
    protected function getHttpMock(): MockObject
    {
        return $this->getServiceMock(DummyHttpClient::class);
    }
}
